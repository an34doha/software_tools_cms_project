from pathlib import Path
import json
import numpy as np
import matplotlib.pyplot as plt

# tqdm is used for a progress bar
from tqdm import tqdm

# The parameters are read by file "parameters.json"
with open("parameters.json", "r") as f:
    PARAMETERS = json.load(f)

# parameters
GRAVITATIONAL_CONSTANT = PARAMETERS["GRAVITATIONAL_CONSTANT"]  # m^3 kg^-1 s^-2
POSITIONS = np.array(PARAMETERS["POSITIONS"])
VELOCITIES = np.array(PARAMETERS["VELOCITIES"])
MASSES = [4 / PARAMETERS["GRAVITATIONAL_CONSTANT"], 4 / PARAMETERS["GRAVITATIONAL_CONSTANT"]]
TIME_STEP = PARAMETERS["TIME_STEP"]  # s
NUMBER_OF_TIME_STEPS = PARAMETERS["NUMBER_OF_TIME_STEPS"]
PLOT_INTERVAL = PARAMETERS["PLOT_INTERVAL"]
masses = np.array([MASSES[1], MASSES[0]])

# derived variables
number_of_planets = len(POSITIONS)
number_of_dimensions = 2

# make sure the number of planets is the same for all quantities
assert len(POSITIONS) == len(VELOCITIES) == len(MASSES)
for position in POSITIONS:
    assert len(position) == number_of_dimensions
for velocity in POSITIONS:
    assert len(velocity) == number_of_dimensions


for step in tqdm(range(NUMBER_OF_TIME_STEPS + 1)):
    # plotting every single configuration does not make sense
    
    if step % PLOT_INTERVAL == 0:              
        fig, ax = plt.subplots()
        x = []
        y = []
        for position in POSITIONS:
            x.append(position[0])
            y.append(position[1])
        ax.scatter(x, y)
        ax.set_aspect("equal")
        ax.set_xlim(-1.5, 1.5)
        ax.set_ylim(-1.5, 1.5)
        ax.set_title("t = {:8.4f} s".format(step * TIME_STEP))
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        output_file_path = Path("positions", "{:016d}.png".format(step))
        output_file_path.parent.mkdir(exist_ok=True)
        fig.savefig(output_file_path)
        plt.close(fig)

    # the accelerations for each planet are required to update the velocities
    # for loops are replaced with NumPy.

    distance_vector = POSITIONS[0] - POSITIONS[1]

    distance_vector_length = np.linalg.norm(distance_vector)
            
    accelerations = GRAVITATIONAL_CONSTANT * masses / distance_vector_length ** 2 * np.array([-1 * distance_vector, distance_vector]) 


    # Semi-implicit Euler method
    VELOCITIES = VELOCITIES + TIME_STEP * accelerations    
    POSITIONS = POSITIONS + TIME_STEP * VELOCITIES
    
